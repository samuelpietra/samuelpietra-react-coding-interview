import { Box, Typography, Avatar, TextField, Button } from '@mui/material';
import { SystemStyleObject, Theme } from '@mui/system';

import { Card } from '@components/atoms';
import { IContact } from 'react-coding-interview-shared/models';
import { useState } from 'react';

export interface IContactCardProps {
  person: IContact;
  sx?: SystemStyleObject<Theme>;
}

export const ContactCard: React.FC<IContactCardProps> = ({
  person: { name, email },
  sx,
}) => {
  const [newName, setNewName] = useState(name);
  const [canEditName, setCanEditName] = useState(true);
  const [newEmail, setNewEmail] = useState(email);
  const [canEditEmail, setCanEditEmail] = useState(true);

  return (
    <Card sx={sx}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar />

        <Box display="flex" flexDirection="row" textAlign="center" mt={2}>
          <TextField
            disabled={canEditName}
            label="Name"
            onChange={(e) => setNewName(e.target.value)}
            value={newName}
          />

          {canEditName ? (
            <Button onClick={() => setCanEditName(false)}>Edit</Button>
          ) : (
            <>
              <Button onClick={() => setCanEditName(true)}>Save</Button>
              <Button onClick={() => setNewName(name)}>Discard</Button>
            </>
          )}
        </Box>

        <Box display="flex" flexDirection="row" textAlign="center" mt={2}>
          <TextField
            disabled={canEditEmail}
            label="Email"
            onChange={(e) => setNewEmail(e.target.value)}
            value={newEmail}
          />

          {canEditEmail ? (
            <Button onClick={() => setCanEditEmail(false)}>Edit</Button>
          ) : (
            <>
              <Button onClick={() => setCanEditEmail(true)}>Save</Button>
              <Button onClick={() => setNewEmail(email)}>Discard</Button>
            </>
          )}
        </Box>
      </Box>
    </Card>
  );
};
